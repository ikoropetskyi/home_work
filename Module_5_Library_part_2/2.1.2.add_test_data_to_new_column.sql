﻿use I_Koropetskyi_Library;

update Publishers
set book_amount=Publisher_Id*10,
	issue_amount=Publisher_Id*15,
	total_edition=Publisher_Id*1000,
	country=(case when Publisher_Id%2=0 then 'Canada' else 'USA' end),
	City=(case when Publisher_Id%2=0 then 'Ottawa' else 'NY' end);


update Authors
set birthday=DATEADD(year,Author_id,'19500617'),
	book_amount=Author_Id*5,
	issue_amount=Author_Id*10,
	total_edition=Author_Id*1000;

update Books
set edition=Publisher_Id*1000,
	published=DATEADD(year,Publisher_id,'19910404'),
	issue=(case when Publisher_Id<6 then 1 else 2 end);


