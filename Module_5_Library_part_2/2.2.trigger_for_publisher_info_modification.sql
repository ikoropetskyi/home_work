﻿USE I_Koropetskyi_Library;
GO

DROP TRIGGER IF EXISTS book_amount_definition_insert_delete;
GO

DROP TRIGGER IF EXISTS book_amount_definition_update;
GO



CREATE trigger book_amount_definition_insert_delete ON Books
AFTER INSERT, DELETE
AS
BEGIN

	UPDATE Publishers
	SET Publishers.book_amount=COALESCE((SELECT COUNT(*) FROM Books
	                            WHERE Books.Publisher_Id=Publishers.Publisher_Id),0)
	WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION ALL SELECT Publisher_Id FROM deleted);

	UPDATE Publishers
	SET Publishers.issue_amount=COALESCE((SELECT COUNT(*) FROM Books
	                            WHERE Books.Publisher_Id=Publishers.Publisher_Id),0)
	WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION ALL SELECT Publisher_Id FROM deleted);

	UPDATE Publishers
	SET Publishers.total_edition=COALESCE((SELECT SUM(edition) FROM Books
	                            WHERE Books.Publisher_Id=Publishers.Publisher_Id),0)
	WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION ALL SELECT Publisher_Id FROM deleted);
END
go


CREATE trigger book_amount_definition_update ON Books
AFTER update
AS
BEGIN
	if update(edition) or update(Publisher_id)
	begin

	UPDATE Publishers
	SET Publishers.book_amount=COALESCE((SELECT COUNT(*) FROM Books
	                            WHERE Books.Publisher_Id=Publishers.Publisher_Id),0)
	WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION ALL SELECT Publisher_Id FROM deleted);

	UPDATE Publishers
	SET Publishers.issue_amount=COALESCE((SELECT COUNT(*) FROM Books
	                            WHERE Books.Publisher_Id=Publishers.Publisher_Id),0)
	WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION ALL SELECT Publisher_Id FROM deleted);

	UPDATE Publishers
	SET Publishers.total_edition=COALESCE((SELECT SUM(edition) FROM Books
	                            WHERE Books.Publisher_Id=Publishers.Publisher_Id),0)
	WHERE Publishers.Publisher_Id IN (SELECT Publisher_Id FROM inserted UNION ALL SELECT Publisher_Id FROM deleted);
	end
END