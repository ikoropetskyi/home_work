﻿USE I_Koropetskyi_Library;
GO

DROP TRIGGER IF EXISTS book_amount_definition_for_authors_bookauthors;
GO

DROP TRIGGER IF EXISTS book_amount_definition_for_authors_books_update;
GO

CREATE trigger book_amount_definition_for_authors_bookauthors ON BooksAuthors
AFTER delete, insert, update
AS
BEGIN
	UPDATE Authors
	SET Authors.book_amount=COALESCE((SELECT COUNT(*) FROM Books as b
	                                  inner join BooksAuthors as ba
									    on b.ISBN=ba.ISBN and ba.Author_id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (select Author_id from inserted
								
								UNION ALL
								
								select Author_id from deleted);
	UPDATE Authors
	SET Authors.issue_amount=COALESCE((SELECT COUNT(*) FROM Books as b
	                                  inner join BooksAuthors as ba
									    on b.ISBN=ba.ISBN and ba.Author_id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (select Author_id from inserted
								
								UNION ALL
								
								select Author_id from deleted);

	UPDATE Authors
	SET Authors.total_edition=COALESCE((SELECT SUM(b.edition) FROM Books as b
	                                  inner join BooksAuthors as ba
									    on b.ISBN=ba.ISBN and ba.Author_id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (select Author_id from inserted
								
								UNION ALL
								
								select Author_id from deleted);

end
go

CREATE trigger book_amount_definition_for_authors_books_update ON Books
AFTER UPDATE
AS
BEGIN

	if update(ISBN) or update(edition)
	begin

	UPDATE Authors
	SET Authors.book_amount=COALESCE((SELECT COUNT(*) FROM Books as b
	                                  inner join BooksAuthors as ba
									    on b.ISBN=ba.ISBN and ba.Author_id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (SELECT * FROM(SELECT ba.Author_id FROM 
								inserted as i inner join BooksAuthors as ba
								on i.ISBN=ba.ISBN) as INS 
								
								UNION ALL
								
								SELECT * FROM(SELECT ba.Author_id FROM 
								deleted as d inner join BooksAuthors as ba
								on d.ISBN=ba.ISBN) as DEL);

    UPDATE Authors
	SET Authors.issue_amount=COALESCE((SELECT COUNT(*) FROM Books as b
	                                  inner join BooksAuthors as ba
									    on b.ISBN=ba.ISBN and ba.Author_id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (SELECT * FROM(SELECT ba.Author_id FROM 
								inserted as i inner join BooksAuthors as ba
								on i.ISBN=ba.ISBN) as INS 
								
								UNION ALL
								
								SELECT * FROM(SELECT ba.Author_id FROM 
								deleted as d inner join BooksAuthors as ba
								on d.ISBN=ba.ISBN) as DEL);

	UPDATE Authors
	SET Authors.total_edition=COALESCE((SELECT SUM(b.edition) FROM Books as b
	                                  inner join BooksAuthors as ba
									    on b.ISBN=ba.ISBN and ba.Author_id=Authors.Author_id),0)

	WHERE Authors.Author_id IN (SELECT * FROM(SELECT ba.Author_id FROM 
								inserted as i inner join BooksAuthors as ba
								on i.ISBN=ba.ISBN) as INS 
								
								UNION ALL
								
								SELECT * FROM(SELECT ba.Author_id FROM 
								deleted as d inner join BooksAuthors as ba
								on d.ISBN=ba.ISBN) as DEL);

    
	end
	
END
