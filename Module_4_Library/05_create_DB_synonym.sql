﻿USE I_Koropetskyi_Library;
GO

DROP SYNONYM IF EXISTS I_Koropetskyi_Library_synonym;
GO

CREATE SYNONYM I_Koropetskyi_Library_synonym FOR I_Koropetskyi_Library;
GO
