use I_Koropetskyi_Library;
go

drop trigger if exists trg_forbid_delete; 
go

create trigger trg_forbid_delete ON Authors_log
after delete
as
begin
rollback tran;
end;