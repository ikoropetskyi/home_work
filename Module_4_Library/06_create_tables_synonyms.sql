﻿USE I_Koropetskyi_Library;
GO

DROP SYNONYM IF EXISTS Authors_synonym;
DROP SYNONYM IF EXISTS Books_synonym;
DROP SYNONYM IF EXISTS BooksAuthors_synonym;
DROP SYNONYM IF EXISTS Publishers_synonym;
DROP SYNONYM IF EXISTS Authors_log_synonym;
GO

CREATE SYNONYM dbo.Authors_synonym FOR I_Koropetskyi_Library.dbo.Authors;
GO

CREATE SYNONYM dbo.Books_synonym FOR I_Koropetskyi_Library.dbo.Books;
GO

CREATE SYNONYM dbo.BooksAuthors_synonym FOR I_Koropetskyi_Library.dbo.BooksAuthors;
GO

CREATE SYNONYM dbo.Publishers_synonym FOR I_Koropetskyi_Library.dbo.Publishers;
GO

CREATE SYNONYM dbo.Authors_log_synonym FOR I_Koropetskyi_Library.dbo.Authors_log;
GO
