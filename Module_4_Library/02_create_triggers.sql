﻿use I_Koropetskyi_Library;
go

--create trigger for Authors_log_table_population

drop trigger if exists trg_populate_authors_logs 
go

create trigger trg_populate_authors_logs ON Authors
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end

---- insert
if @operation = 'I'
begin
	insert into Authors_log
				(Author_Id_new,Name_new,URL_new,operation_type)
	Select 
		inserted.Author_Id,
		inserted.Name,
		inserted.URL,
		@operation
	FROM Authors 
	inner join inserted on Authors.Author_Id = inserted.Author_Id
end

---- delete
if @operation = 'D'
begin
	insert into Authors_log
				(Author_Id_old,Name_old,URL_old,operation_type)
	Select 
		deleted.Author_Id,
		deleted.Name,
		deleted.URL,
		@operation
	FROM deleted
end

--- update
if @operation = 'U'
	begin
		update Authors 
		set updated = CURRENT_TIMESTAMP, 
			updated_by = SYSTEM_USER
		from Authors 
			inner join inserted on Authors.Author_Id = inserted.Author_Id;

	insert into Authors_log
				(Author_Id_new,Name_new,URL_new,Author_Id_old,Name_old,URL_old,operation_type)
	Select 
		inserted.Author_Id,
		inserted.Name,
		inserted.URL,
		deleted.Author_Id,
		deleted.Name,
		deleted.URL,
		@operation
	FROM inserted INNER JOIN deleted ON inserted.Author_Id = deleted.Author_Id
		
	end
END;
GO


--create triggers for Books updations

drop trigger if exists trg_books_update
go
create trigger trg_books_update ON Books
AFTER UPDATE
AS
BEGIN

IF @@ROWCOUNT = 0 RETURN
update Books 
		set updated = CURRENT_TIMESTAMP, 
			updated_by = SYSTEM_USER
		from Books
			inner join inserted on Books.ISBN = inserted.ISBN

END;
GO

--create triggers for BooksAuthors updations

drop trigger if exists trg_BooksAuthors_update
go

create trigger trg_BooksAuthors_update ON BooksAuthors
AFTER UPDATE
AS
BEGIN

IF @@ROWCOUNT = 0 RETURN
update BooksAuthors
		set updated = CURRENT_TIMESTAMP, 
			updated_by = SYSTEM_USER
		from BooksAuthors
			inner join inserted on BooksAuthors.BooksAuthors_id = inserted.BooksAuthors_id

END;
GO

--create triggers for Publishers updations

drop trigger if exists trg_Publishers_update
go

create trigger trg_Publishers_update ON Publishers
AFTER UPDATE
AS
BEGIN

IF @@ROWCOUNT = 0 RETURN
update Publishers
		set updated = CURRENT_TIMESTAMP, 
			updated_by = SYSTEM_USER
		from Publishers
			inner join inserted on Publishers.Publisher_Id = inserted.Publisher_Id

END;
GO



