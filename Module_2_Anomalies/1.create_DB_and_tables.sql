﻿--create db and tables for ts testing

drop database if exists ts_anomalies;
go

create database ts_anomalies;
go

use ts_anomalies;

drop table if exists products;
go

create table products
(id int not null identity(1,1) primary key,
[name] varchar(15) not null,
price numeric(5,2) not null
);
go

insert into products ([name], price)
values ('product_1',10),
	   ('product_2',100),
	   ('product_3',200);


