﻿--dirty read
--we have 2 connections

--switch to first connection
use ts_anomalies;
go

select * from products
where id=1;

begin tran;
 update products
   set price+=20
   where id=1;

 select * from products
 where id=1;
 
--switch to second connection
--tran is not committed yet

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
use ts_anomalies;
go

select * from products
where id=1;
--we have read uncomitted changes

--switch to first connection
rollback tran

--SOLUTION
--still in connection 1
use ts_anomalies;
go

select * from products
where id=1;

begin tran;
 update products
   set price+=30
   where id=1;

 select * from products
 where id=1;

--switch to second connection
--tran is not committed yet

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
use ts_anomalies;
go

select * from products
where id=1;

--ts is blocked, so we cant read uncommited changes

--back to 1-st connection
commit tran

--in second connection ts is completed

