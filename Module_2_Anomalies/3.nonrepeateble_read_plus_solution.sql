--non repeateble read
--we have 2 connections

--switch to first connection
use ts_anomalies;
go

begin tran;
select * from products
where id=2;

--ts is not comitted

--switch to second connection
use ts_anomalies;
go

begin tran;
update products
set price+=200
where id=2
commit tran;

--switch to first connection
select * from products
where id=2;

commit tran;

--we have 2 different result in one ts, not repeatable read

--SOLUTION
SET TRANSACTION ISOLATION LEVEL REPEATABLE READ;
use ts_anomalies;
go

begin tran;
select * from products
where id=2;

--ts is not comitted

--switch to second connection
use ts_anomalies;
go

begin tran;
update products
set price+=200
where id=2
commit tran;

--ts is blocked

--switch to first connection
select * from products
where id=2;

commit tran;
--we have the same result as in first read
--now updates are applied
