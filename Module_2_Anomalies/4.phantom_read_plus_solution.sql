--phantoms
--we have 2 connections

--switch to first connection
use ts_anomalies;
go

begin tran;
select * from products
where name in('product_1','product_2');

--ts is not comitted

--switch to second connection

use ts_anomalies;
go

insert into products([name],price)
values('product_1',500);

--switch to first connection

select * from products
where name in('product_1','product_2');
commit tran;
--we see this phantom row within 1 ts

--SOLUTION
--switch to first connection

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

use ts_anomalies;
go

begin tran;
select * from products
where name in('product_1','product_2');

--ts is not comitted

--switch to second connection

use ts_anomalies;
go

insert into products([name],price)
values('product_1',600);

--ts is blocked

--switch to first connection
select * from products
where name in('product_1','product_2');

--we dont see phantom row
commit tran
--now insertion is completed

