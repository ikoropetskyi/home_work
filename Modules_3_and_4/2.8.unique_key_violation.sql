﻿--choose database
USE I_K_module_3;
GO

--UNIQUE KEY VIOLATION
--inn 1111111111 ALREADY EXISTS

INSERT INTO HR.employee(id,inn,passport,first_name,last_name,middle_name,date_of_birth,
			  date_of_hire,date_of_fire,department,position,salary)
VALUES (4,'1111111111','КС333333','Андрій','Візнович','Іванович','19950106','20100626',NULL,'Маркетинг','маркетолог',12000.00);

--select from main table
SELECT * FROM HR.employee;

--SELECT FROM audit table
SELECT * FROM HR.employee_audit;

GO