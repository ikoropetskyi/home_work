﻿--choose DB
USE I_K_module_3;
GO

DROP VIEW IF EXISTS [dbo].[owner_info];
DROP VIEW IF EXISTS [dbo].[car_info];
GO

--view for dbo.owner table
CREATE VIEW [dbo].[owner_info](passport,first_name,last_name,city) 
AS
SELECT passport,first_name,last_name,city
FROM [dbo].[owner];
GO

----view for dbo.car table
CREATE VIEW [dbo].[car_info](registration_number,mark,color) 
AS
SELECT registration_number,mark,color
FROM [dbo].[car];
GO

--select data from views
SELECT *
FROM [dbo].[owner_info];
GO

SELECT *
FROM [dbo].[car_info];
GO
