﻿--CHOOSE DB
USE I_K_module_3;
GO

DROP TRIGGER IF EXISTS tr_insert_employee;
DROP TRIGGER IF EXISTS tr_delete_employee;
DROP TRIGGER IF EXISTS tr_update_employee;
GO

--CREATE TRIGGER FOR INSERT
CREATE TRIGGER tr_insert_employee ON HR.employee
AFTER INSERT
AS
BEGIN

  IF @@ROWCOUNT=0 RETURN;

  INSERT INTO HR.employee_audit(id,inn,passport,first_name,last_name,middle_name,date_of_birth,
			  date_of_hire,date_of_fire,department,position,salary,operation_type,operation_date)
  SELECT id,inn,passport,first_name,last_name,middle_name,date_of_birth,
			date_of_hire,date_of_fire,department,position,salary,'insert',CURRENT_TIMESTAMP
  FROM inserted;

END;
GO

--CREATE TRIGGER FOR DELETE
CREATE TRIGGER tr_delete_employee ON HR.employee
AFTER DELETE
AS
BEGIN

  IF @@ROWCOUNT=0 RETURN;

  INSERT INTO HR.employee_audit(id,inn,passport,first_name,last_name,middle_name,date_of_birth,
			  date_of_hire,date_of_fire,department,position,salary,operation_type,operation_date)
  SELECT id,inn,passport,first_name,last_name,middle_name,date_of_birth,
			date_of_hire,date_of_fire,department,position,salary,'delete',CURRENT_TIMESTAMP
  FROM deleted;

END;
GO

--CREATE TRIGGER FOR UPDATE
CREATE TRIGGER tr_update_employee ON HR.employee
AFTER UPDATE
AS
BEGIN

  IF @@ROWCOUNT=0 RETURN;

  INSERT INTO HR.employee_audit(id,inn,passport,first_name,last_name,middle_name,date_of_birth,
			  date_of_hire,date_of_fire,department,position,salary,operation_type,operation_date)
  SELECT id,inn,passport,first_name,last_name,middle_name,date_of_birth,
			date_of_hire,date_of_fire,department,position,salary,'update',CURRENT_TIMESTAMP
  FROM inserted;

END;
GO