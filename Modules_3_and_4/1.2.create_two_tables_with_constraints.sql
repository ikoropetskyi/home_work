﻿--choose database
USE I_K_module_3;
GO

--drop tables if they already exist
DROP TABLE IF EXISTS [dbo].[Owner];
DROP TABLE IF EXISTS [dbo].[Car];
GO

--create parent table
CREATE TABLE [dbo].[Owner]
  (
  id NCHAR(10) NOT NULL CONSTRAINT PK_Owner_Id PRIMARY KEY,
  passport NCHAR(8) NOT NULL UNIQUE,
  first_name NVARCHAR(20) NOT NULL,
  last_name NVARCHAR(30) NOT NULL,
  middle_name NVARCHAR(25) NULL,
  date_of_birth DATE NOT NULL CHECK(date_of_birth<=CURRENT_TIMESTAMP),
  city NVARCHAR(20) NOT NULL,
  street_name NVARCHAR(30) NOT NULL,
  street_number NVARCHAR(7) NOT NULL,
  license_number NCHAR(9) NULL,
  license_date_from DATE NULL,
  inserted_date DATETIME NOT NULL DEFAULT(CURRENT_TIMESTAMP),
  updated_date DATETIME NULL,
  year_of_driving_expirience AS DATEDIFF(year, license_date_from, CURRENT_TIMESTAMP)
  );

--create child table
CREATE TABLE [dbo].[Car]
  (
  id NCHAR (17) NOT NULL,
  registration_number NCHAR(8) NOT NULL,
  owner_id NCHAR(10) NOT NULL,
  mark NVARCHAR(20) NOT NULL,
  body_type NVARCHAR(20) NOT NULL,
  engine_capicity SMALLINT NOT NULL CHECK(engine_capicity>0),
  door_number TINYINT NOT NULL DEFAULT(5) CHECK(door_number>=0),
  cylinder_number TINYINT NOT NULL,
  fuel_type NVARCHAR(15) NOT NULL,
  color NVARCHAR(20) NOT NULL,
  registration_date DATE NOT NULL,
  inserted_date DATETIME NOT NULL DEFAULT(CURRENT_TIMESTAMP),
  updated_date DATETIME NULL,
  CONSTRAINT PK_Car_Id PRIMARY KEY(id),
  CONSTRAINT AK_Car_Registration_Number UNIQUE(registration_number),
  CONSTRAINT CK_Car_Cylinder_Number CHECK(cylinder_number>0)
  );
  GO
