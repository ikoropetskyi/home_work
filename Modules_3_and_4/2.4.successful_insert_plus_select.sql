﻿--choose database
USE I_K_module_3;
GO

--successful insert into table
INSERT INTO HR.employee(id,inn,passport,first_name,last_name,middle_name,date_of_birth,
			  date_of_hire,date_of_fire,department,position,salary)
VALUES (1,'1111111111','КС111111','Ігор','Коропецький','Петрович','19910101','20080521',NULL,'Аналітики','аналітик',10000.00),
       (2,'2222222222','КС222222','Андрій','Балацький','Іванович','19950106','20100626',NULL,'Маркетинг','маркетолог',12000.00)

--select from main table
SELECT * FROM HR.employee;

--SELECT FROM audit table
SELECT * FROM HR.employee_audit;

GO