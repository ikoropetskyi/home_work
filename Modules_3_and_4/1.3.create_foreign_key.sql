﻿--choose_database
USE I_K_module_3;
GO

--create foreign key
ALTER TABLE [dbo].[Car]
  ADD CONSTRAINT FK_Owner_Id
  FOREIGN KEY(owner_id) REFERENCES [dbo].[Owner](id)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
GO