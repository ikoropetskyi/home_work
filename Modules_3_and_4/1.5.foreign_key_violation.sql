﻿--choose database
USE I_K_module_3;
GO

--failed insert into table dbo.Car
--owner_id does not exist in parent table
--foreign key violation
INSERT INTO [dbo].[Car] (id, registration_number, owner_id, mark, body_type, engine_capicity,
                         door_number, cylinder_number, fuel_type, color,
						 registration_date, updated_date)
VALUES ('YBFKSDSJ23N2FІERT','ВС3575ЛР','1111111111','DEO Lanos','седан',1400,
        4,12,'бензин','білий','20120409',Null);
GO