﻿USE education;
GO

DROP SYNONYM IF EXISTS I_Koropetskyi.emp;
DROP SYNONYM IF EXISTS I_Koropetskyi.emp_audit;
DROP SCHEMA IF EXISTS I_Koropetskyi;
GO

CREATE SCHEMA I_Koropetskyi;
GO

CREATE SYNONYM I_Koropetskyi.emp FOR I_K_module_3.HR.employee;
GO

CREATE SYNONYM I_Koropetskyi.emp_audit FOR I_K_module_3.HR.employee_audit;
GO