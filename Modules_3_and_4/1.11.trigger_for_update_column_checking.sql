﻿--choose DB
USE I_K_module_3;
GO

--select from owner before update
SELECT * FROM [dbo].[owner];
GO

--updation
UPDATE [dbo].[owner]
SET street_number='1111'
WHERE passport='KC330000';
GO

--select from owner after update
SELECT * FROM [dbo].[owner];
GO

---------------------------------
--select from car before update
SELECT * FROM [dbo].[car];
GO

--updation
UPDATE [dbo].[car]
SET cylinder_number=100
WHERE id='EBFKSDSJ23N2FІВІВ';
GO

--select from car after update
SELECT * FROM [dbo].[car];
GO