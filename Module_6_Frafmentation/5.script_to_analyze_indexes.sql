﻿use people_ua_db;
select i.[name] as [index], object_name(s.[object_id]) as 'table', s.[index_id], 
	   s.[page_count], s.[index_type_desc],s.[avg_fragmentation_in_percent], 
	   s.[avg_fragment_size_in_pages],s.[avg_page_space_used_in_percent],
       s.[fragment_count] 
from sys.dm_db_index_physical_stats(DB_ID('people_ua_db'), NULL, NULL, NULL, 'SAMPLED') as s
  inner join sys.indexes as i  
	on  s.[object_id] = i.[object_id] and s.[index_id]=i.[index_id]
where object_name(s.[object_id]) not in ('women_names','men_names','name_list_identity','surname_list_identity');
