﻿use people_ua_db;


declare @index_name as nvarchar(50),
        @table_name as nvarchar(50),
		@frag_percent as float,
		@sql as varchar(max);

declare c cursor for
(select i.[name] as index_name, object_name(s.[object_id]) as table_name, s.[avg_fragmentation_in_percent] as frag_percent
from sys.dm_db_index_physical_stats(DB_ID('people_ua_db'), NULL, NULL, NULL, 'SAMPLED') as s
  inner join sys.indexes as i  
	on  s.[object_id] = i.[object_id] and s.[index_id]=i.[index_id]
where object_name(s.[object_id]) not in ('women_names','men_names','name_list_identity','surname_list_identity'));

open c;
fetch next from c into @index_name, @table_name, @frag_percent
while @@FETCH_STATUS=0
  begin
    set @sql='alter index '+@index_name+' on '+@table_name;
	if @frag_percent>5 and @frag_percent<30
	  begin
	    exec(@sql+' reorganize')
	  end
    if @frag_percent>=30
	  begin
	    exec(@sql+' rebuild')
	  end
    fetch next from c into @index_name, @table_name, @frag_percent
  end;

close c;
deallocate c;