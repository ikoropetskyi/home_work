﻿--create tables according to logical model + constraints
use people_ua_db;
go

drop table if exists region;
drop table if exists university;
drop table if exists university_graduates;
drop table if exists peoples;
go

create table region
 (id tinyint not null identity,
 name nvarchar(20) not null,
 main_city nvarchar(20) not null,
 part_of_country nvarchar(10) not null,
 number_of_universities int null,
 constraint pk_region primary key nonclustered(id));

create table university
 (id int not null identity,
  name nvarchar(50) not null,
  found date not null,
  region_id tinyint not null,
  constraint pk_university primary key(id));

create table university_graduates
  (id int not null identity,
  people_id int not null,
  university_id int not null,
  graduate_date date not null,
  specialization nvarchar(50) not null,
  constraint pk_graduates primary key(id));

--declare main table
create table peoples
(id int not null identity,
surname nvarchar(20) not null,
[name] nvarchar(20) not null,
sex nchar(1) not null,
birthday date not null,
region_of_birth_id tinyint not null,
constraint pk_peoples primary key nonclustered(id));
go

--create foreign keys for references
alter table peoples
add constraint fk_people_region
foreign key (region_of_birth_id)
references region(id)
on delete cascade
on update cascade;

alter table university
add constraint fk_university_region
foreign key(region_id)
references region(id)
on delete cascade
on update cascade; 

alter table university_graduates
add constraint fk_grad_people
foreign key(people_id)
references peoples(id)
on delete cascade
on update cascade;

alter table university_graduates
add constraint fk_grad_university
foreign key(university_id)
references university(id);
go
