﻿USE [triger_fk_cursor];

INSERT INTO street(street)
VALUES ('Шевченка'),('Франка'),('Л.Українки');

INSERT INTO post(post)
VALUES ('провізор'),('директор');

INSERT INTO pharmacy(name,building_number,www,work_time,saturday,sunday,street)
VALUES
                    ('Podorozhnyk','23',NULL,'20:00',1,0,'Шевченка'),
					('DS','2',NULL,'18:00',1,1,'Франка'),
					('Znahar','43',NULL,'19:00',1,1,'Л.Українки');

INSERT INTO employee(surname,name,midle_name,identity_number,passport,experience,birthday,post,pharmacy_id)
VALUES               
					('Шевченко','Андрій','Андрійович','1111111111','КС111111',2,'19910202','провізор',1),
					('Шевченко','Валентина','Тарасівна','1111111112','КС111112',5,'19810202','директор',1),
					('Франко','Андрій','Андрійович','1111111113','КС111113',3,'19950202','провізор',2),
					('Франко','Микита','Олегович','1111111114','КС111114',1,'19940202','директор',2),
					('Українець','Андрій','Андрійович','1111111115','КС111115',2,'19960202','провізор',3),
					('Українець','Панас','Федорович','1111111116','КС111116',4,'19910502','директор',3),
					('Null_test','Null_test',null,null,null,null,null,'директор',null);

INSERT INTO medicine (name, ministry_code, recipe, narcotic,psychotropic)
VALUES                
					 ('Фіналгон','1111111111',0,0,0),
					 ('Мезим','2222222222',0,0,0),
					 ('Гідрозипам','3333333333',1,0,1),
					 ('Маалокс','4444444444',0,0,0),
					 ('Фурацилін','5555555555',0,0,0);

INSERT INTO zone(name)
VALUES
                 ('Суглоб'),
				 ('Шлунок'),
				 ('Нервова система'),
				 ('Горло');

INSERT INTO pharmacy_medicine(pharmacy_id,medicine_id)
VALUES
                              (1,1),
							  (1,2),
							  (1,3),
							  (1,5),
							  (2,1),
							  (2,3),
							  (2,4),
							  (3,1),
							  (3,2),
							  (3,3),
							  (3,4);

INSERT INTO medicine_zone (medicine_id, zone_id)
VALUES                    
						   (1,1),
						   (2,2),
						   (3,3),
						   (4,2),
						   (5,4);